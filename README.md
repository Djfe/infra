## Allgemeines

Zugang per OpenVPN verwaltet von Shiva

* Subnetz  192.168.100.0/24 IPMI
* Subnetz  192.168.110.0/24 old MGMT-on-WAN
* Subnetz  192.168.111.0/32 MGMT-on-WAN – lokale IPs auf WAN-Interface zum Managementzugang (.11 .12 und .13 derzeit)
* Subnetz  192.168.120.0/24 Ceph

Die folgenden 3 Subnetze werden entfernt und über die Kupfer-Verbindungen läuft Ceph-Only.
Hierzu ist eine zusätzliche Spanning-Tree-Konfiguration oder dergleichen notwendig.
https://pve.proxmox.com/wiki/Full_Mesh_Network_for_Ceph_Server
Management läuft dann über MGMT-on-WAN mit lokalen IPs, die über die Glasfaseranschlüsse geroutet werden. Da hier bereits STP (Spanning Tree Protocol) im Einsatz ist, wie auch für die Public-IP-Adressen, ist somit keine weitere Konfiguration außer dem Anlegen der IPs auf dem Interface notwendig.

* Subnetz  192.168.101.0/24 lokales Netz zw. 1 und 2 - obsolet
* Subnetz  192.168.102.0/24 lokales Netz zw. 2 und 3 - obsolet
* Subnetz  192.168.103.0/24 lokales Netz zw. 3 und 1 - obsolet

## Zugriff (Backend)

| Name                       | Daniel | shiva | Malte  | Arne     | Florian | Djfe | Jannic | Marius | monty  | madez
|----------------------------|--------|-------|--------|----------|---------|------|--------|--------|--------|--------|
| Admin-VPN                  | y      | y     | y      | y        | y       | y    |        | n      |        |        |
| Proxmox                    | n      | y     | y      | y        | y       | y    | n      | n      | n      |        |
| DNS/Hosting.de             |        |       |        | access   | access  | n    | admin  | n      |        |        |
| Mail/Mailcow               |        |       |        | sysadmin |         | n    | admin  | admin  |        |        |
| Supernodes                 | n      | y     | y      | y        | y       | y    | n      | y      | n      | (wg2)  |
| FastD-Supernodes           |        | y     | y      | y        | y       | n    | y      | n      |        |        |
| Update-Frontend            |        | sysadmin|      |          |         | n    |        | n      |        |        |
| WordPress Server           |        | y     | (y)    |          |         | n    |        |        |        |        |
| Zabbix                     | n      | sysadmin| sysadmin | n    | n       | sysadmin | n  | n      |        |        |
| ffac-monit.skydisc.net     | (y)    | y     |        |          | y       | n    |        |        |        |        |
| monitor.freifunk-aachen.de | n      | y     | y      | y        | y       | y    | n      | y      | n      |        |
| grafana.freifunk-aachen.de |        |       |        |          | admin   | editor |      |        |        |        |
| meet.freifunk-aachen.de    | n      | n     | y      | n        | y       | n    | y      | n      |        |        |
| ffac-build.skydisc.net     | y      | y     | y      | n        | y       | y    | y      | n      | n      |        |
| build.freifunk-aachen.de   | n      | y     | y      | y        | y       | y    | n      | n      | n      |        |
| docker01.freifunk-aachen.de |       | y     | y      | y        | y       | y    |        | y      |        |        |
| status.freifunk-aachen.de  |        |       |        |          | y       | y    |        |        |        |        |

## Zugriff (Code, Vereinsdokumente, Kommunikation)

| Name      |                                           | Malte | Stefan | Djfe   | Florian | switzly | Jannic  | xcircle | Arne    | vegms   | Marius  | monty   | yayachiken | Josha | Daniel | shiva 
|-----------|-------------------------------------------|-------|--------|--------|---------|---------|---------|---------|---------|---------|---------|---------|---------|---------|---------|---------|
| Tickets   | https://support.freifunk-aachen.de        | agent | agent  | admin  | admin   | agent   |         | agent   | agent   | agent   | admin   |         |         | agent   | agent   |         |
| GitLab    | https://gitlab.com/f3n                    | owner | maintainer | maintainer | owner | guest | owner | maintainer | maintainer | maintainer | |	  |         |         |         |         |
| GitHub    | https://github.com/ffac                   | owner |        | member | owner   | member  | owner   |         | member  | member  |         | owner   | member  |         |         | owner   |
| Slack     | https://ffac.slack.com/                   | admin | admin  |        |         |         |         |         |         |         |         |         |         |         |         |         |
| Matrix Space | https://matrix.to/#/#ffac-space:matrix.org |   | ?      |        | ?       |         |         |         |         |         |         |         |         |         |         |         |
| WordPress | https://freifunk-aachen.de/wp-login.php   | admin | admin  | admin  | author  | editor  | admin   | editor  |         |         |         |         |         |         |         |         |
| Technik WP | https://technik.freifunk-aachen.de/wp-login.php   | admin | | admin |        |         |         |         | editor  |         |         | editor  | editor  |         |         |         |
| Twitter   | https://twitter.com/FreifunkAachen        | admin |        |        |         |         |         |         |         |         |         |         |         |         |         |         |
| Facebook  | https://www.facebook.com/freifunkaachen   |       |        | editor |         | admin   |         |         |         |         |         |         |         |         |         |         |
| Freifunk Forum | https://forum.freifunk.net/          | account |     | account | account |         |         |         |         |         |         |         |         |         |         |         |
| Ehrenamt Aachen | https://engagementdirekt.aachen.de/ |       | account |       |         |         |         |         |         |         |         |         |         |         |         |         |
| Mastodon  | ?                                         |       |        |        |         |         |         |         |         |         |         |         |         |         |         |         |

Im Freifunk-Forum gehören uns die Kategorien:
https://forum.freifunk.net/c/community/aachen/16
https://forum.freifunk.net/c/technik/regioaachen/94
https://forum.freifunk.net/c/community/herzogenrath/78

## EPYC-Server

|                     | EPYC-01          | EPYC-02              | EPYC-03           |
|---------------------|------------------|----------------------|-------------------|
| IPMI IP             | 192.168.100.1/30 | 192.168.100.5/30     | 192.168.100.9/30  |
| erreicht von        | Epyc-03 enp1s0   | Epyc-01 enp1s0       | Epyc-02 enp1s0    |
| mit zugewiesener IP | 192.168.100.2/30 | 192.168.100.6/30     | 192.168.100.10/30 |
| MAC IPMI            |                  |                      |                   |
| MGMT-WAN (vmbr)     | 192.168.110.11   | 192.168.110.12       | 192.168.110.13    |
| MGMT-WAN (lo:1)     | 192.168.111.11   | 192.168.111.12       | 192.168.111.13    |
| OS                  | Proxmox 8.0.4    | Proxmox 8.0.4        | Proxmox 8.0.4     |
| CPU                 | AMD EPYC 7402P   | AMD EPYC 7402P       | EPYC 7402P        |
| Kerne (Threads)     | 24 (48)          | 24 (48)              | 24 (48)           |
| RAM [GB]            | 128              | 128                  | 128               |
| Storage [GB]        | 2x256NVME+480+480 | 2x256NVME+1600NVME+480+480 | 2x256NVME+480+480 |

### Bridges

| Bridge | Usage                     | EPYC-01           | EPYC-02           | EPYC-03           |
|--------|---------------------------|-------------------|-------------------|-------------------|
| vmbr0  | enp65s0f0np0 enp65s0f1np1 | 192.168.110.11/24 | 192.168.110.12/24 | 192.168.110.13/24 |
| vmbr1  | eno2np1                   | 192.168.103.11/24 | 192.168.102.12/24 | 192.168.102.13/24 |
| vmbr2  | eno1np0                   | 192.168.101.11/24 | 192.168.101.12/24 | 192.168.103.13/24 |
| vmbr3  | IPMI (enp1s0)             | 192.168.100.6/30  | 192.168.100.10/30 | 192.168.100.2/30  |

## IP-Adressen

Freifunk Aachen nutzt derzeit folgende Subnetze:

| Protokoll | Besitzer           | Verwendung         | Subnetz            | Host-Adressbereich                     | Broadcast-Adresse | Gateway        |
|-----------|--------------------|--------------------|--------------------|----------------------------------------|-------------------|----------------|
| IPv4      | Relaix             | Infrastruktur      | 5.145.135.128/27   | 5.145.135.129 - 5.145.135.158          | 5.145.135.159     |                |
| IPv6      | Relaix             | Infrastruktur      | 2a00:fe0:43::/48   | 2a00:fe0:43:: - 2a00:fe0:43:ffff::     |                   | 2a00:fe0:43::1 |
| IPv4      | Freifunk Rheinland | Nodes              | 185.66.193.40/29   | 185.66.193.41 - 185.66.193.46          | 185.66.193.47     |                |
| IPv6      | Freifunk Rheinland | Nodes              | 2a03:2260:114::/48 | 2a03:2260:114:: - 2a03:2260:114:ffff:: |                   |                |
| IPv6      | Freifunk Rheinland | Nodes?             | 2a03:2260:3006::/48 | 2a03:2260:3006:: - 2a03:2260:3006:ffff:: |                |                |
| IPv4      | Privat             | Ceph, Intern       | 192.168.102.0/24   | 192.168.102.1 - 192.168.102.254        | 192.168.102.255   |                |
| IPv4      | Privat             | IPMI               | 192.168.100.0/24   | 192.168.100.1 - 192.168.100.254        | 192.168.100.255   |                |


| IPv4-Adresse | Name             | DNS                                |
|---------------|-----------------|------------------------------------|
| 5.145.135.129 | Gateway         | -                                  |
| 5.145.135.131 | FastD-Node01    | 01.nodes.freifunk-aachen.de        |
| 5.145.135.132 | FastD-Node02    | 02.nodes.freifunk-aachen.de        |
| 5.145.135.133 | FastD-Node03    | 03.nodes.freifunk-aachen.de        |
| 5.145.135.134 | FastD-Node04    | 04.nodes.freifunk-aachen.de        |
| 5.145.135.135 | FastD-Node05    | 05.nodes.freifunk-aachen.de        |
| 5.145.135.136 | FastD-Node06    | 06.nodes.freifunk-aachen.de        |
| 5.145.135.137 | -               |                                    |
| 5.145.135.138 | Build-Server    | community-build.freifunk-aachen.de |
| 5.145.135.139 | Jitsi           | meet.freifunk-aachen.de            |
| 5.145.135.140 | AdminVPN01      | admvpn1.ffac.rocks                 |
| 5.145.135.141 | FFRL-Router01   | -                                  |
| 5.145.135.142 | FFRL-Router02   | -                                  |
| 5.145.135.143 | -               | -                                  |
| 5.145.135.148 | Monitor         | monitor.freifunk-aachen.de         |
| 5.145.135.149 | Docker 01       | docker01.freifunk-aachen.de        |
| 5.145.135.151 | WG-Node01       | 01.wg-node.freifunk-aachen.de      |
| 5.145.135.152 | WG-Node02       | 02.wg-node.freifunk-aachen.de      |
| 5.145.135.153 | ?WG-Node03      | ?03.wg-node.freifunk-aachen.de     |
| 5.145.135.154 | -               | -                                  |
| 5.145.135.155 | -               | -                                  |
| 5.145.135.156 | -               | -                                  |
| 5.145.135.157 | -               | -                                  |
| 5.145.135.158 | -               | -                                  |
| 46.183.100.204| alter Build     | ffac-build.skydisc.net             |
| 46.183.100.205| altes Monit     | ffac-monit.skydisc.net             |
| 185.66.193.41 | FastD-Node01    | -                                  |
| 185.66.193.42 | FastD-Node02    | -                                  |
| 185.66.193.43 | FastD-Node03    | -                                  |
| 185.66.193.44 | FastD-Node04    | -                                  |
| 185.66.193.45 | FastD-Node05    | nicht gerouted                     |
| 185.66.193.46 | FastD-Node06    | nicht gerouted                     |


| IPv6-Adresse         | Name         | DNS                                |
|-----------------------|--------------|------------------------------------|
| 2a00:fe0:43::1        | Gateway      | -                                  |
| 2a00:fe0:43::bb:1     | FFRL-Router01 | -                                 |
| 2a00:fe0:43::bb:2     | FFRL-Router02 | -                                 |
| 2a00:fe0:43::101      | FastD-Node01 | 01.nodes.freifunk-aachen.de        |
| 2a00:fe0:43::2        | FastD-Node02 | 02.nodes.freifunk-aachen.de        |
| 2a00:fe0:43::3        | FastD-Node03 | 03.nodes.freifunk-aachen.de        |
| 2a00:fe0:43::4        | FastD-Node04 | 04.nodes.freifunk-aachen.de        |
| 2a00:fe0:43::5        | FastD-Node05 | 05.nodes.freifunk-aachen.de        |
| 2a00:fe0:43::6        | FastD-Node06 | 06.nodes.freifunk-aachen.de        |
| 2a00:fe0:43::139      | Jitsi        | meet.freifunk-aachen.de            |
| 2a00:fe0:43::148      | Monitor      | monitor.freifunk-aachen.de         |
| 2a00:fe0:43::149      | Docker 01    | docker01.freifunk-aachen.de        |
| 2a00:fe0:43::affe:1   | SSH-JumpHost | -                                  |
| 2a00:fe0:43::affe:2   | SSH-JumpHost | -                                  |
| 2a00:fe0:43::affe:3   | SSH-JumpHost | -                                  |
| 2a00:fe0:43::ffac:140 | VPN-HAProxy  | -                                  |
| 2a00:fe0:43::ffac:3   | VPN-Router03 | -                                  |
| 2a00:fe0:43::ffac:2   | VPN-Router02 | -                                  |
| 2a00:fe0:43::ffac:1   | VPN-Router01 | -                                  |
| 2a00:fe0:43::201      | WG-Node01    | 01.wg-node.freifunk-aachen.de      |
| 2a00:fe0:43::202      | WG-Node02    | 02.wg-node.freifunk-aachen.de      |
| 2a00:fe0:43::203      | ?WG-Node03   | ?03.wg-node.freifunk-aachen.de     |
| 2a00:fe0:43::ffac:b0  | Build-Server | build.freifunk-aachen.de           |
| 2a00:fe0:1:36::3      | alter Build  | ffac-build.skydisc.net             |
| 2a00:fe0:1:36::2      | altes Monit  | ffac-monit.skydisc.net             |

## Netzwerk

Folgendes Draw-IO Diagramm beschreibt unsere aktuelle Netzwerk-Verkabelung bei RELAIX:

![Network](./network-drawio.svg)
